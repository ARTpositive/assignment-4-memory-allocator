#define _DEFAULT_SOURCE

#include "mem.h"
#include "mem_internals.h"

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>

struct block_header* header_from_data(void* data){
    return (struct block_header*)(data - offsetof(struct block_header, contents));
}

// Helper function to check if memory is allocated and its capacity
void check_allocation(void* mem, size_t expected_capacity) {
    assert(mem != NULL);
    size_t actual_capacity = header_from_data(mem)->capacity.bytes;
    assert(actual_capacity >= expected_capacity);
}

// Helper function to free memory and check if it's properly marked free
void free_and_check(void* mem) {
    _free(mem);
    assert(header_from_data(mem)->is_free);
}

void test_alloc_free() {
    printf("Running allocation tests!\n");
    heap_init(REGION_MIN_SIZE);

    void* mem1 = _malloc(16);
    void* mem2 = _malloc(2048);
    void* mem3 = _malloc(8192);

    check_allocation(mem1, 16);
    check_allocation(mem2, 2048);
    check_allocation(mem3, 8192);

    free_and_check(mem1);
    free_and_check(mem2);
    free_and_check(mem3);

    heap_term();
    printf("Allocation test finished!\n");
}

void test_region_extend() {
    printf("Running region extending test!\n");
    heap_init(REGION_MIN_SIZE);

    size_t region_size = REGION_MIN_SIZE;
    void* mem1 = _malloc(region_size / 2);
    void* mem2 = _malloc(region_size);
    void* mem3 = _malloc(region_size / 2);

    check_allocation(mem1, region_size / 2);
    check_allocation(mem2, region_size);
    check_allocation(mem3, region_size / 2);

    // Verify the blocks are properly linked
    assert(header_from_data(mem1)->next == header_from_data(mem2));
    assert(header_from_data(mem2)->next == header_from_data(mem3));

    assert(mem1 + header_from_data(mem1)->capacity.bytes == header_from_data(mem2));
    assert(mem2 + header_from_data(mem2)->capacity.bytes == header_from_data(mem3));

    free_and_check(mem1);
    free_and_check(mem2);
    free_and_check(mem3);

    heap_term();
    printf("Region extending test finished!\n");
}

void test_region_intersection() {
    printf("Running region intersection test!\n");

    size_t size = getpagesize() * 2 - offsetof(struct block_header, contents);
    heap_init(size);

    void* mem1 = _malloc(size);

    int* res = mmap((void*) HEAP_START + getpagesize() * 2, getpagesize() * 2, PROT_READ | PROT_WRITE,
			MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
    assert(res != MAP_FAILED);

    void* mem2 = _malloc(REGION_MIN_SIZE);
    void* mem3 = _malloc(REGION_MIN_SIZE / 2);

    // Test allocations
    check_allocation(mem1, size);
    check_allocation(mem2, REGION_MIN_SIZE);
    check_allocation(mem3, REGION_MIN_SIZE / 2);

    // Ensure the blocks are not overlapping inappropriately
    assert(header_from_data(mem1)->next == header_from_data(mem2));
    assert(mem1 + header_from_data(mem1)->capacity.bytes != mem2);

    // Free memory and check
    free_and_check(mem1);
    free_and_check(mem2);
    free_and_check(mem3);

    // Unmap memory region used for testing intersection
    munmap((void*) HEAP_START + getpagesize() * 2, getpagesize() * 2);
    heap_term();
    printf("Region intersection test finished!\n");
}

void empty_func(){
	printf("empty");
}

int main() {
    // Run the test
    test_alloc_free();
    test_region_extend();
    test_region_intersection();

    printf("All tests passed!\n");
    return 0;
}

